// RPC stubs for clients to talk to extent_server

#include "extent_client.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

using std::string;

// The calls assume that the caller holds a lock on the extent

extent_client::extent_client(std::string dst)
{
  sockaddr_in dstsock;
  make_sockaddr(dst.c_str(), &dstsock);
  cl = new rpcc(dstsock);
  if (cl->bind() != 0) {
    printf("extent_client: bind failed\n");
  }
}

extent_protocol::status
extent_client::get(extent_protocol::extentid_t eid, std::string &buf)
{
  // Assume we have got the lock on eid.
  // 1. Check cache.

  base::ScopedLock l(&cache_map_m_);
  printf("Cache GET %016llx\n", eid);

  if (extent_cache_map_.count(eid) == 0) {
    // eid does not exist in cache, get from server.
    // Cache attribute
    extent_protocol::status ret;
    ret = get_cache_attr(eid, extent_cache_map_[eid].attr);
    if (ret != extent_protocol::OK) { return ret; }
  }

  // 2. Cache exist, check removed and NOENT.
  ExtentFileCache& file_cache = extent_cache_map_[eid];
  if (file_cache.state == REMOVED || file_cache.state == NOENT) {
    return extent_protocol::NOENT;
  }

  // 3. Check buffer.
  if (file_cache.buf == NULL) {
    string* file_buf = new string();
    extent_protocol::status ret;
    ret = cl->call(extent_protocol::get, eid, *file_buf);
    printf("call server to get buffer. %016llx\n", eid);
    if (ret != extent_protocol::OK) return ret;
    file_cache.buf = file_buf; // buffer created.
    printf("created buffer. %016llx\n", eid);
  }

  buf = *file_cache.buf; // copy to return
  return extent_protocol::OK;
}

extent_protocol::status
extent_client::getattr(extent_protocol::extentid_t eid,
		       extent_protocol::attr &attr)
{

  base::ScopedLock l(&cache_map_m_);

  if (extent_cache_map_.count(eid) == 0) {
    // eid does not exist in cache, get from server.
    // Cache attribute
    extent_protocol::status ret;
    ret = get_cache_attr(eid, extent_cache_map_[eid].attr);
    if (ret != extent_protocol::OK) { return ret; }
  }

  ExtentFileCache& file_cache = extent_cache_map_[eid];
  if (file_cache.state == REMOVED || file_cache.state == NOENT) {
    return extent_protocol::NOENT;
  }

  attr = file_cache.attr;
  return extent_protocol::OK;
}

extent_protocol::status
extent_client::put(extent_protocol::extentid_t eid, std::string buf)
{
  base::ScopedLock l(&cache_map_m_);
  printf("Cache PUT %016llx\n", eid);

  time_t now = time(NULL);
  if (extent_cache_map_.count(eid) > 0 && extent_cache_map_[eid].buf != NULL) {
      delete extent_cache_map_[eid].buf;
      extent_cache_map_[eid].buf = NULL;
  }

  // Construct ExtentFileCache if needed.
  ExtentFileCache &file_cache = extent_cache_map_[eid];

  // set attr.
  file_cache.attr.atime = file_cache.attr.mtime = file_cache.attr.ctime = now;
  file_cache.attr.size = buf.length();

  std::string *file_buf = new std::string(buf);
  file_cache.buf = file_buf;

  // flag.
  file_cache.state = DIRTY;

  return extent_protocol::OK;
}

extent_protocol::status
extent_client::remove(extent_protocol::extentid_t eid)
{
  base::ScopedLock l(&cache_map_m_);

  if (extent_cache_map_.count(eid) > 0) {
    if (extent_cache_map_[eid].buf != NULL) {
      delete extent_cache_map_[eid].buf;
      extent_cache_map_[eid].buf = NULL;
    }
  } else {
    // No cache, get cache.
    extent_protocol::status ret;
    ret = get_cache_attr(eid, extent_cache_map_[eid].attr);
    if (ret != extent_protocol::OK) { return ret; }
  }

  // Have cache and file_cache.buf == NULL.
  ExtentFileCache &file_cache = extent_cache_map_[eid];
  if (file_cache.state == REMOVED || file_cache.state == NOENT) {
    return extent_protocol::NOENT;
  }

  extent_cache_map_[eid].state = REMOVED;
  return extent_protocol::OK;
}

extent_protocol::status
extent_client::get_cache_attr(extent_protocol::extentid_t eid,
		       extent_protocol::attr &attr)
{
  // Without mutex.
  extent_protocol::status ret;
  ret = cl->call(extent_protocol::getattr, eid, attr);
  if (ret != extent_protocol::OK) {
    // Cache NOENT
    if (ret == extent_protocol::NOENT) {
      extent_cache_map_[eid].state = NOENT;
    } else {
      extent_cache_map_.erase(eid);
    }
  } else {
    extent_cache_map_[eid].attr = attr; // Cache
  }
  return ret;
}

void extent_client::flush(extent_protocol::extentid_t eid) {
  printf("Flushing %016llx\n", eid);
  base::ScopedLock l(&cache_map_m_);
  if (extent_cache_map_.count(eid) > 0) {
    ExtentFileCache& file_cache = extent_cache_map_[eid];

    if (file_cache.state == DIRTY) {
      extent_protocol::status ret;
      int r;
      ret = cl->call(extent_protocol::put, eid, *file_cache.buf, r);
      VERIFY (ret == extent_protocol::OK);

    } else if (file_cache.state == REMOVED) {

      extent_protocol::status ret;
      int r;
      ret = cl->call(extent_protocol::remove, eid, r);
      VERIFY (ret == extent_protocol::OK);

    }
    extent_cache_map_.erase(eid);
  }
}

