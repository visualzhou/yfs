// extent client interface.

#ifndef extent_client_h
#define extent_client_h

#include <string>
#include <map>
#include <ctime> // time()
#include "extent_protocol.h"
#include "lock.hpp"
#include "rpc.h"

class extent_client {
public:
  extent_client(std::string dst);

  extent_protocol::status get(extent_protocol::extentid_t eid,
			      std::string &buf);
  extent_protocol::status getattr(extent_protocol::extentid_t eid,
				  extent_protocol::attr &a);
  extent_protocol::status put(extent_protocol::extentid_t eid, std::string buf);
  extent_protocol::status remove(extent_protocol::extentid_t eid);
  void flush(extent_protocol::extentid_t eid);

private:
  enum FileState { READONLY=0, NOENT, DIRTY, REMOVED }; // READONLY is the normal one.
  struct ExtentFileCache : public ExtentFile {
    ExtentFileCache() : state(READONLY) { buf = NULL; }
    FileState state;
  };

  rpcc *cl;

  std::map<extent_protocol::extentid_t, ExtentFileCache> extent_cache_map_;
  base::Mutex cache_map_m_;

  extent_protocol::status
    get_cache_attr(extent_protocol::extentid_t eid,
        extent_protocol::attr &attr);
};

#endif

