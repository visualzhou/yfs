#ifndef lock_server_cache_h
#define lock_server_cache_h

#include <string>
#include <queue>
#include <list>
#include <tr1/unordered_map>
#include "lock_protocol.h"
#include "rpc.h"
#include "lock_server.h"

using std::string;
using std::tr1::unordered_map;
using std::queue;
using std::list;

class lock_server_cache {
public:
  lock_server_cache();
  lock_protocol::status stat(lock_protocol::lockid_t, int &);
  int acquire(lock_protocol::lockid_t, std::string id, int &);
  int release(lock_protocol::lockid_t, std::string id, int &);

private:
  struct CachedLock {
    string owner;
    queue<string, list<string> > waiting_queue;
  };

  int nacquire;
  unordered_map<lock_protocol::lockid_t, CachedLock> lock_map_;
  Mutex map_m_;
  ConditionVar map_cv_;

  rlock_protocol::status revoke_client(lock_protocol::lockid_t lid, string& id);
  rlock_protocol::status retry_client(lock_protocol::lockid_t lid, string& id);
};

#endif
