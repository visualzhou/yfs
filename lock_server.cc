// the lock server implementation

#include "lock_server.h"
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>

lock_server::lock_server():
  nacquire (0)
{
}

lock_protocol::status
lock_server::stat(int clt, lock_protocol::lockid_t lid, int &r)
{
  lock_protocol::status ret = lock_protocol::OK;
  printf("stat request from clt %d\n", clt);
  r = nacquire;
  return ret;
}

lock_protocol::status
lock_server::acquire(int clt, lock_protocol::lockid_t lid, int &r) {
  base::ScopedLock l(&map_m_);
  while (lock_map_.count(lid) > 0) {
    // Lid exists.
    map_cv_.wait(&map_m_);
  }
  // New lid.
  lock_map_[lid] = clt;
  nacquire++;
  return lock_protocol::OK;
}

lock_protocol::status
lock_server::release(int clt, lock_protocol::lockid_t lid, int &r) {
  base::ScopedLock l(&map_m_);
  if (lock_map_.count(lid) == 0) {
    // Error: release an unlock mutex.
    return lock_protocol::NOENT;
  }
  else if (lock_map_[lid] == clt) {
    lock_map_.erase(lid);
    map_cv_.signalAll();
  }
  else {
    // Error: release a mutex that doesn't belongs to clt.
    return lock_protocol::NOENT;
  }
  return lock_protocol::OK;
}
