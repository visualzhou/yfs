// yfs client.  implements FS operations using extent and lock server
#include "yfs_client.h"
#include "extent_client.h"
#include "lock_client.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using std::istringstream;
using std::ostringstream;

const yfs_client::inum yfs_client::ROOT = 0x00000001;

yfs_client::yfs_client(string extent_dst, string lock_dst)
  : ec_(extent_dst), extent_flush_cb_(&ec_),
    lock_client_(lock_dst, &extent_flush_cb_) {

  DLock dl(&lock_client_, ROOT);

  if (!exists(ROOT, &dl)) {
    make_root();
  }
}

yfs_client::inum
yfs_client::n2i(string n)
{
  istringstream ist(n);
  unsigned long long finum;
  ist >> finum;
  return finum;
}

string
yfs_client::filename(inum inum)
{
  ostringstream ost;
  ost << inum;
  return ost.str();
}

bool
yfs_client::isfile(inum inum)
{
  if(inum & 0x80000000)
    return true;
  return false;
}

bool
yfs_client::isdir(inum inum)
{
  return ! isfile(inum);
}

int
yfs_client::getfile(inum inum, fileinfo &fin)
{
  int r = OK;

  printf("getfile %016llx\n", inum);
  extent_protocol::attr a;
  if (ec_.getattr(inum, a) != extent_protocol::OK) {
    r = IOERR;
    goto release;
  }

  fin.atime = a.atime;
  fin.mtime = a.mtime;
  fin.ctime = a.ctime;
  fin.size = a.size;
  printf("getfile %016llx -> sz %llu\n", inum, fin.size);

release:

  return r;
}

int
yfs_client::getdir(inum inum, dirinfo &din)
{
  int r = OK;
  // You modify this function for Lab 3
  // - hold and release the directory lock

  printf("getdir %016llx\n", inum);
  extent_protocol::attr a;
  if (ec_.getattr(inum, a) != extent_protocol::OK) {
    r = IOERR;
    goto release;
  }
  din.atime = a.atime;
  din.mtime = a.mtime;
  din.ctime = a.ctime;

release:
  return r;
}

//
// A file/directory's attributes are a set of information
// including owner, permissions, size, &c. The information is
// much the same as that returned by the stat() system call.
// The kernel needs attributes in many situations, and some
// fuse functions (such as lookup) need to return attributes
// as well as other information, so getattr() gets called a lot.
//
// YFS fakes most of the attributes. It does provide more or
// less correct values for the access/modify/change times
// (atime, mtime, and ctime), and correct values for file sizes.
//
yfs_client::status yfs_client::getattr(yfs_client::inum inum, struct stat &st)
{
  DLock dl(&lock_client_, inum);
  return getattr_internal(inum, st);
}

yfs_client::status yfs_client::getattr_internal(yfs_client::inum inum, struct stat &st)
{
  yfs_client::status ret;

  bzero(&st, sizeof(st));

  st.st_ino = inum;
  printf("getattr %016llx %d\n", inum, isfile(inum));
  if(isfile(inum)){
     fileinfo info;
     ret = getfile(inum, info);
     if(ret != OK)
       return ret;
     st.st_mode = S_IFREG | 0666;
     st.st_nlink = 1;
     st.st_atime = info.atime;
     st.st_mtime = info.mtime;
     st.st_ctime = info.ctime;
     st.st_size = info.size;
     printf("   getattr -> %llu\n", info.size);
   } else {
     dirinfo info;
     ret = getdir(inum, info);
     if(ret != OK)
       return ret;
     st.st_mode = S_IFDIR | 0777;
     st.st_nlink = 2;
     st.st_atime = info.atime;
     st.st_mtime = info.mtime;
     st.st_ctime = info.ctime;
     printf("   getattr -> %lu %lu %lu\n", info.atime, info.mtime, info.ctime);
   }
   return OK;
}

bool
yfs_client::exists(inum id, DLock *dl) {
  extent_protocol::attr attr;
  if (dl != NULL) {
    return ec_.getattr(id, attr) == extent_protocol::OK;
  } else {
    DLock dlock(&lock_client_, id);
    return ec_.getattr(id, attr) == extent_protocol::OK;
  }
}

int
yfs_client::make_root() {
  string str;
  return ec_.put(ROOT, str);
}

yfs_client::status
yfs_client::create(inum parent, const char *name, inum *ino, struct stat *st) {
  return create(parent, name, ino, st, true);
}

// Create file @name in directory @parent.
//
// TODO: get and put, wrap with dlock.
yfs_client::status
yfs_client::create(inum parent, const char *name, inum *ino, struct stat *st,
    bool file) {
  Directory dir;

  // Distributed lock.
  DLock dl(&lock_client_, parent);

  yfs_client::status ret = readdir(parent, &dir, &dl);
  if (ret != yfs_client::OK) return ret;

  // If a file named @name already exists in @parent, return EXIST.
  if (dir.contains(name)) {
    return EXIST;
  }

  // printf("Creating file. picking up new ino in parent %016llx\n", parent);
  // Pick an ino (with type of yfs_client::inum) for file/dir @name.
  do {
    *ino = random();
    if (file) {
      // Make sure ino indicates a file, not a directory!
      *ino |= 0x80000000;
    } else {
      // Make sure ino indicates a directory!
      *ino &= 0x7fffffff;
    }
  } while (exists(*ino));

  // Create an empty extent for ino.
  // Empty file and empty dir are the same.
  string empty;

  // lock ino for ec cache.
  DLock dl_ino(&lock_client_, *ino);

  ret = ec_.put(*ino, empty);
  if (ret != extent_protocol::OK) return ret;

  // Add a <name, ino> entry into @parent.
  dir.add_file(*ino, name);

  std::cout << dir.to_str();
  // Change the parent's mtime and ctime to the current time/date
  // (this may fall naturally out of your extent server code).
  ret = ec_.put(parent, dir.to_str());
  if (ret != extent_protocol::OK) return ret;
  // getattr_internal
  return getattr_internal(*ino, *st);
}

yfs_client::status
yfs_client::lookup(inum parent, const char *name, inum *ino, struct stat *st) {
  Directory dir;

  DLock dl(&lock_client_, parent);
  yfs_client::status ret = readdir(parent, &dir, &dl);
  if (ret != yfs_client::OK) return ret;

  // Lookup
  if (!dir.lookup(name, ino)) {
    return NOENT;
  }

  DLock dl_ino(&lock_client_, *ino);
  return getattr_internal(*ino, *st);
}

yfs_client::status
yfs_client::read(inum ino, string *buf, size_t off, size_t size) {
  string content;

  DLock dl(&lock_client_, ino);
  int ret = ec_.get(ino, content);
  if (ret != extent_protocol::OK) return ret;
  buf->clear();
  buf->append(content, off, size);
  return yfs_client::OK;
}

// TODO: get and put, wrap with dlock.
yfs_client::status yfs_client::resize(inum ino, size_t size, struct stat *st) {

  // Distributed lock.
  DLock dl(&lock_client_, ino);

  string content;
  int ret = ec_.get(ino, content);
  if (ret != extent_protocol::OK) return ret;
  content.resize(size);
  ret = ec_.put(ino, content);
  if (ret != extent_protocol::OK) return ret;

  // getattr
  return getattr_internal(ino, *st);
}

// TODO: get and put, wrap with dlock.
yfs_client::status
yfs_client::write(inum ino, const char *buf, size_t off, size_t size) {
  // Distributed lock.
  DLock dl(&lock_client_, ino);

  string content;
  int ret = ec_.get(ino, content);
  if (ret != extent_protocol::OK) return ret;
  if (off < content.length()) {
    // replace
    // string& replace ( size_t pos1, size_t n1, const char * s, size_t n2 );
    content.replace(off, size, buf, size);
  } else {
    // append
    content.resize(off);
    content.append(buf, size);
  }

  ret = ec_.put(ino, content);
  if (ret != extent_protocol::OK) return ret;
  return yfs_client::OK;
}

yfs_client::status yfs_client::readdir(inum ino, Directory *dir, DLock *dlock) {
  // Check parent is dir.
  if (!isdir(ino)) {
    return NOENT;
  }

  // Get dir.
  string content;
  int ret;
  if (dlock) {
    ret = ec_.get(ino, content);
  } else {
    DLock dl(&lock_client_, ino);
    ret = ec_.get(ino, content);
  }
  if (ret != extent_protocol::OK) return ret;
  dir->parse_dir(content);
  return OK;
}

yfs_client::status
yfs_client::mkdir(inum parent, const char *name, inum *ino, struct stat *st) {
  return create(parent, name, ino, st, false);
}

// TODO: get and put, wrap with dlock.
yfs_client::status
yfs_client::unlink(inum parent, const char *name) {
  // Distributed lock.
  DLock dl(&lock_client_, parent);

  Directory dir;
  yfs_client::status ret = readdir(parent, &dir, &dl);
  if (ret != yfs_client::OK) return ret;

  // Remove extent
  inum ino;
  if (!dir.lookup(name, &ino)) {
    return NOENT;
  }
  dir.remove(name);

  // Put back to extent server.
  ret = ec_.put(parent, dir.to_str());
  if (ret != extent_protocol::OK) return ret;

  DLock dl_ino(&lock_client_, ino);
  ret = ec_.remove(ino);
  if (ret != extent_protocol::OK) return ret;
  return OK;
}

/*****************************************
 * Directory
*****************************************/

Directory::Directory(string& content) {
  parse_dir(content);
}

// Deserialization
void Directory::parse_dir(string& content) {
  istringstream ist(content);
  while (!ist.eof()) {
    yfs_client::inum ino;
    char c; // Consume the space between ino and name.
    string name;
    // 10 foo.txt
    ist >> ino;
    ist.get(c);
    std::getline(ist, name); // readline
    if (name.length() > 0) {
      entry_map_[name] = ino; // Not empty.
    }
  }
}

// Serialization
string Directory::to_str() {
  ostringstream ost;
  map<string, yfs_client::inum>::iterator it;
  for (it = entry_map_.begin(); it != entry_map_.end(); it++) {
    ost << (*it).second << ' ' << (*it).first << std::endl;
  }
  return ost.str();
}

bool Directory::contains(const char* file_name) {
  yfs_client::inum ino;
  return lookup(file_name, &ino);
}

bool Directory::lookup(const char* file_name, yfs_client::inum *ino) {
  string name(file_name); // convert to string
  if (entry_map_.count(name) == 0) {
    return false;
  }

  *ino = entry_map_[name];
  return true;
}

void Directory::add_file(yfs_client::inum ino, const char* name) {
  string n(name);
  entry_map_[n] = ino;
}

// TODO: use iterator
void Directory::to_vector(vector<yfs_client::dirent> *v) {
  map<string, yfs_client::inum>::iterator it;
  for (it = entry_map_.begin(); it != entry_map_.end(); it++) {
    yfs_client::dirent ent;
    ent.name = it->first;
    ent.inum = it->second;
    v->push_back(ent);
  }
}

bool Directory::remove(const char* file_name) {
  string n(file_name);
  return entry_map_.erase(n);
}
