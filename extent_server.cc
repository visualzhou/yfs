// the extent server implementation

#include "extent_server.h"
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

extent_server::extent_server() {}


int extent_server::put(extent_protocol::extentid_t id, std::string buf, int &)
{
  // TODO: change buf to &buf.
  base::ScopedLock l(&extent_map_m_);
  printf("PUT %016llx\n", id);
  // std::cout << buf << std::endl;

  time_t now = time(NULL);
  if (extent_map_.count(id) > 0) {
    // update.
    delete extent_map_[id].buf;
  }

  ExtentFile &file = extent_map_[id];

  // set attr.
  file.attr.atime = file.attr.mtime = file.attr.ctime = now;
  file.attr.size = buf.length();

  std::string *file_buf = new std::string(buf);
  file.buf = file_buf;

  return extent_protocol::OK;
}

int extent_server::get(extent_protocol::extentid_t id, std::string &buf)
{
  base::ScopedLock l(&extent_map_m_);
  printf("GET %016llx\n", id);

  if (extent_map_.count(id) == 0) {
    // id does not exist.
    return extent_protocol::NOENT;
  }

  ExtentFile &file = extent_map_[id];
  file.attr.atime = time(NULL);

  buf = *file.buf; // copy to return
  return extent_protocol::OK;
}

int extent_server::getattr(extent_protocol::extentid_t id, extent_protocol::attr &a)
{
  // You fill this in for Lab 2.
  // You replace this with a real implementation. We send a phony response
  // for now because it's difficult to get FUSE to do anything (including
  // unmount) if getattr fails.
  base::ScopedLock l(&extent_map_m_);

  if (extent_map_.count(id) == 0) {
    // id does not exist.
    printf("GETATTR %016llx Fail\n", id);
    return extent_protocol::NOENT;
  }

  a = extent_map_[id].attr;
  printf("GETATTR %016llx Success\n", id);
  return extent_protocol::OK;
}

int extent_server::remove(extent_protocol::extentid_t id, int &)
{
  base::ScopedLock l(&extent_map_m_);

  if (extent_map_.count(id) == 0) {
    // id does not exist.
    return extent_protocol::NOENT;
  }

  delete extent_map_[id].buf;
  extent_map_.erase(id);

  return extent_protocol::OK;
}

