// lock client interface.

#ifndef lock_client_cache_h

#define lock_client_cache_h

#include <string>
#include <tr1/unordered_map>
#include "lock_protocol.h"
#include "rpc.h"
#include "lock_client.h"
#include "lang/verify.h"

#include "lock.hpp"

using std::tr1::unordered_map;

// Classes that inherit lock_release_user can override dorelease so that
// that they will be called when lock_client releases a lock.
// You will not need to do anything with this class until Lab 5.
class lock_release_user {
public:
  virtual void dorelease(lock_protocol::lockid_t) = 0;
  virtual ~lock_release_user() {};
};

class lock_client_cache : public lock_client {
public:
  lock_client_cache(std::string xdst, class lock_release_user *l = 0);
  virtual ~lock_client_cache() {};
  lock_protocol::status acquire(lock_protocol::lockid_t);
  lock_protocol::status release(lock_protocol::lockid_t);
  rlock_protocol::status revoke_handler(lock_protocol::lockid_t, int &);
  rlock_protocol::status retry_handler(lock_protocol::lockid_t, int &);

private:
  enum CacheState { FREE, LOCKED, ACQUIRING, RELEASING, FREE_RELEASING };
  class lock_release_user *lu;
  int rlock_port;
  std::string hostname;
  std::string id;
  unordered_map<lock_protocol::lockid_t, CacheState> lock_cache_map_;
  base::Mutex cache_map_m_;
  base::ConditionVar cache_map_cv_;

  void release_to_server(lock_protocol::lockid_t lid);
};


#endif
