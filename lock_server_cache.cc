// the caching lock server implementation

#include "lock_server_cache.h"
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "lang/verify.h"
#include "handle.h"
#include "tprintf.h"

lock_server_cache::lock_server_cache()
{
}


int lock_server_cache::acquire(lock_protocol::lockid_t lid, std::string id, int &)
{
  nacquire++;
  map_m_.lock();
  if (lock_map_.count(lid) == 0) {
    // The server is the owner. No cache.
    lock_map_[lid].owner = id;
    map_m_.unlock();
    return lock_protocol::OK;
  }

  // The lock is cached.
  CachedLock& lock = lock_map_[lid];
  bool first = lock.waiting_queue.empty();

  // Enqueue.
  lock.waiting_queue.push(id);

  // Mutex is unlocked.
  map_m_.unlock();

  if (first) {
    // This client is the first one.
    // Revoke the owner.
    rlock_protocol::status ret = revoke_client(lid, lock.owner);
    if (ret != rlock_protocol::OK) {
      return ret;
    }
  }
  // Send RETRY
  return lock_protocol::RETRY;
}

int
lock_server_cache::release(lock_protocol::lockid_t lid, std::string id, int &r)
{
  map_m_.lock();

  CachedLock& lock = lock_map_[lid];
  // Replace the owner.
  lock.owner = lock.waiting_queue.front();
  lock.waiting_queue.pop();
  bool revoke_owner = !lock.waiting_queue.empty();

  map_m_.unlock();

  rlock_protocol::status ret;
  ret = retry_client(lid, lock.owner);
  if (ret != rlock_protocol::OK) {
    tprintf("Release: retry next owner error %d", ret);
  }

  if (revoke_owner) {
    ret = revoke_client(lid, lock.owner);
    if (ret != rlock_protocol::OK) {
      tprintf("Release: revoke next owner error %d", ret);
    }
  }

  // Return OK anyway.
  return lock_protocol::OK;
}

lock_protocol::status
lock_server_cache::stat(lock_protocol::lockid_t lid, int &r)
{
  tprintf("stat request\n");
  r = nacquire;
  return lock_protocol::OK;
}

rlock_protocol::status
lock_server_cache::revoke_client(lock_protocol::lockid_t lid, string& id) {
  handle h(id);
  rlock_protocol::status ret;
  if (h.safebind()) {
    int r;
    ret = h.safebind()->call(rlock_protocol::revoke, lid, r);
    return ret;
  } else{
    // handle failure
    return rlock_protocol::RPCERR;
  }
}

rlock_protocol::status
lock_server_cache::retry_client(lock_protocol::lockid_t lid, string& id) {
  handle h(id);
  rlock_protocol::status ret;
  if (h.safebind()) {
    int r;
    ret = h.safebind()->call(rlock_protocol::retry, lid, r);
    return ret;
  } else{
    // handle failure
    return rlock_protocol::RPCERR;
  }
}
