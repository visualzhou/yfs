#ifndef yfs_client_h
#define yfs_client_h

#define LOCK_CACHE 1
#include <string>
#include <vector>
#include <map>

//#include "yfs_protocol.h"
#include "extent_client.h"
#include "lock_protocol.h"
#if LOCK_CACHE
  #include "lock_client_cache.h"
#else
  #include "lock_client.h"
#endif

using std::string;
using std::map;
using std::vector;

class Directory; // Declare first.

class ExtentFlushCallback : public lock_release_user {
public:
  ExtentFlushCallback(extent_client *ec) : ec_(ec) { }
  void dorelease(lock_protocol::lockid_t lid) {
    ec_->flush(lid);
  }

private:
  extent_client *ec_;
};

class yfs_client {
public:

  /* Data stuctures */
  typedef unsigned long long inum;
  const static inum ROOT;
  enum xxstatus { OK, RPCERR, NOENT, IOERR, EXIST };
  typedef int status;

  struct fileinfo {
    unsigned long long size;
    unsigned long atime;
    unsigned long mtime;
    unsigned long ctime;
  };
  struct dirinfo {
    unsigned long atime;
    unsigned long mtime;
    unsigned long ctime;
  };
  struct dirent {
    string name;
    yfs_client::inum inum;
  };

  /* Fucntions */
  yfs_client(string, string);

  bool isfile(inum);
  bool isdir(inum);

  int getfile(inum, fileinfo &);
  int getdir(inum, dirinfo &);

  bool exists(inum, DLock* dl = NULL);

  /* File system functions in fuse */
  status getattr(yfs_client::inum inum, struct stat &st);
  status create(inum parent, const char *name, inum *ino, struct stat *st);
  status lookup(inum parent, const char *name, inum *ino, struct stat *st);
  status read(inum ino, string *buf, size_t off, size_t size);
  status write(inum ino, const char *buf, size_t off, size_t size);
  status resize(inum ino, size_t size, struct stat *st);

  // Directory
  status readdir(inum ino, Directory *dir, DLock *dlock = NULL);
  status mkdir(inum parent, const char *name, inum *ino, struct stat *st);
  status unlink(inum parent, const char *name);

private:
  extent_client ec_;
  ExtentFlushCallback extent_flush_cb_;

#if LOCK_CACHE
  lock_client_cache lock_client_;
#else
  lock_client lock_client_;
#endif

  static string filename(inum);
  static inum n2i(string);

  int make_root();
  status create(inum parent, const char *name, inum *ino, struct stat *st,
      bool file);
  status getattr_internal(yfs_client::inum inum, struct stat &st);
};

// File name cannot contain
// \n Directory format depends on \n as delimiter
// \0 File name is passed in as char*
class Directory {
public:
  Directory() { };
  Directory(string&);

  void parse_dir(string&);

  bool contains(const char* file_name);
  bool lookup(const char* file_name, yfs_client::inum*);
  void add_file(yfs_client::inum ino, const char* name);
  void to_vector(vector<yfs_client::dirent> *v);
  // Return true if success.
  bool remove(const char* file_name);

  string to_str();

private:

  map<string, yfs_client::inum> entry_map_;

};

#endif
