// RPC stubs for clients to talk to lock_server, and cache the locks
// see lock_client.cache.h for protocol details.

#include "lock_client_cache.h"
#include "rpc.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include "tprintf.h"

using std::string;

lock_client_cache::lock_client_cache(std::string xdst,
				     class lock_release_user *_lu)
  : lock_client(xdst), lu(_lu)
{
  rpcs *rlsrpc = new rpcs(0); // Get a random port.
  rlsrpc->reg(rlock_protocol::revoke, this, &lock_client_cache::revoke_handler);
  rlsrpc->reg(rlock_protocol::retry, this, &lock_client_cache::retry_handler);

  const char *hname;
  hname = "127.0.0.1";
  std::ostringstream host;
  host << hname << ":" << rlsrpc->port();
  id = host.str();
}

lock_protocol::status
lock_client_cache::acquire(lock_protocol::lockid_t lid)
{
  base::ScopedLock l(&cache_map_m_);
  while (true) {
    while (lock_cache_map_.count(lid) > 0
        && lock_cache_map_[lid] != FREE
        && lock_cache_map_[lid] != FREE_RELEASING) {
      // I am the owner, but the lock is not avaible now.
      cache_map_cv_.wait(&cache_map_m_);
    }

    if (lock_cache_map_.count(lid) > 0) {
      // I am the owner and the lock is free / releasing (available).
      if (lock_cache_map_[lid] == FREE) {
        lock_cache_map_[lid] = LOCKED;
      } else {
        // Keep RELEASING. FREE_RELEASING -> RELEASING
        lock_cache_map_[lid] = RELEASING;
      }
      break;
    }

    // Acquire the lock from server.
    // 1. Set ACQUIRING to prevent other threads to send RPC.
    lock_cache_map_[lid] = ACQUIRING;
    // 2. Now it is safe to unlock.
    cache_map_m_.unlock();
    // 3. Send RPC.
    int r;
    lock_protocol::status ret = cl->call(lock_protocol::acquire, lid, id, r);
    if (ret != lock_protocol::OK && ret != lock_protocol::RETRY) {
      VERIFY(0); // Someting goes wrong.
    }
    // 4. Grab mutex.
    cache_map_m_.lock();
    // 5. Response OK.
    if (ret == lock_protocol::OK) {
      // Revoke may arrive earlier. In that case, we keep RELEASING.
      if (lock_cache_map_[lid] == ACQUIRING) {
        lock_cache_map_[lid] = LOCKED; // Got lock,
      }
      break;
    }
    // 6. RETRY. Wait for retry_handler() or other release() to signal.
    // That says, the thread sending RPC may not be the one who gets the lock.
  }
  return lock_protocol::OK;
}

lock_protocol::status
lock_client_cache::release(lock_protocol::lockid_t lid)
{
  cache_map_m_.lock();

  bool send_release = false;

  if (lock_cache_map_[lid] == LOCKED) {
    lock_cache_map_[lid] = FREE;
  } else if (lock_cache_map_[lid] == RELEASING) {
    // Release
    lock_cache_map_.erase(lid);
    send_release = true;
  }

  // Wake up threads on the same lock to get free lock or acquire new one.
  cache_map_cv_.signalAll();
  cache_map_m_.unlock();

  if (send_release) {
    release_to_server(lid);
  }
  return lock_protocol::OK;
}

rlock_protocol::status
lock_client_cache::revoke_handler(lock_protocol::lockid_t lid, int &)
{
  // TODO: Use return value to indicate whether the lock is free.
  bool send_release;
  { base::ScopedLock l(&cache_map_m_);

    send_release = (lock_cache_map_[lid] == FREE);
    if (send_release) {
      // Send release RPC to server.
      lock_cache_map_.erase(lid);
    } else {
      lock_cache_map_[lid] = RELEASING;
    }
  } // Exit ScopedLock

  if (send_release) {
    release_to_server(lid);
  }
  return rlock_protocol::OK;
}

rlock_protocol::status
lock_client_cache::retry_handler(lock_protocol::lockid_t lid, int &)
{
  base::ScopedLock l(&cache_map_m_);

  if (lock_cache_map_[lid] == ACQUIRING) {
    lock_cache_map_[lid] = FREE;
  } else {
    // Keep RELEASING
    lock_cache_map_[lid] = FREE_RELEASING;
  }

  cache_map_cv_.signalAll();

  int ret = rlock_protocol::OK;
  return ret;
}

void lock_client_cache::release_to_server(lock_protocol::lockid_t lid) {
  // Clean cache before releasing lock in extent client cache.
  if (lu != NULL) lu->dorelease(lid);

  int r;
  lock_protocol::status ret = cl->call(lock_protocol::release, lid, id, r);
  VERIFY (ret == lock_protocol::OK);
}

