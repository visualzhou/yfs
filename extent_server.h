// this is the extent server

#ifndef extent_server_h
#define extent_server_h

#include <string>
#include <map>
#include <ctime> // time()
#include "extent_protocol.h"
#include "lock.hpp"

class extent_server {

public:
  extent_server();

  int put(extent_protocol::extentid_t id, std::string, int &);
  int get(extent_protocol::extentid_t id, std::string &);
  int getattr(extent_protocol::extentid_t id, extent_protocol::attr &);
  int remove(extent_protocol::extentid_t id, int &);

private:

  std::map<extent_protocol::extentid_t, ExtentFile> extent_map_;
  base::Mutex extent_map_m_;

};

#endif







